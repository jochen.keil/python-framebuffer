from time import time_ns
from PIL import ImageDraw
from Framebuffer import Image

FB_DEV = "/dev/fb1"

try:
  with Image(FB_DEV) as fbi:
    img = fbi.get_image()

    draw = ImageDraw.Draw(img)

    xres = fbi.get_vscreeninfo().xres
    yres = fbi.get_vscreeninfo().yres

    start = time_ns()

    n = 0
    frames = 1E3
    while n < frames:
      if n % 2 == 0:
        draw.rectangle([0, 0, xres, yres], fill='white')
      else:
        draw.rectangle([0, 0, xres, yres], fill='black')

      n = n + 1

    stop = time_ns()

    # ATTENTION: absolutely necessary!
    del(draw)

    fps = frames / ((stop - start) / 1E9)
    print(f'FPS: {fps}')

except Exception as e:
  print(e)
