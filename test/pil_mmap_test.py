from mmap import mmap
from random import randint
from PIL import __version__, Image, ImageDraw, sys
from Framebuffer import Framebuffer, image, draw, FBImage, MMapImage, mmap_image

# create test file:
# dd if=/dev/zero of=test.mmap bs=1 count=$((128*64))

print('PIL.__version__', __version__)

XSIZE = 128
YSIZE = 64

try:
  # with MMapImage('test.mmap', (XSIZE, YSIZE), 'L') as img:
  with mmap_image('test.mmap', (XSIZE, YSIZE), 'L') as img:
    # fb = Framebuffer('test.mmap', 0)
    # mm = fb.mmap()
    print('got mmap')

    size = (XSIZE, YSIZE)
    # with image(fb, size) as img:
    # with Image.frombuffer(data=fb.mmap(), size=size, mode='L') as img:
      # img.readonly = False
    # with Image.frombuffer(data=fb.mmap(), size=size, mode='I;16') as img:
    # img = FBImage(fb, (XSIZE, YSIZE)).img()
    # img = fbi.img()

    # img = Image.frombuffer(data=mm, size=(XSIZE, YSIZE), mode='L')
    print(f'got img (refcount: {sys.getrefcount(img)})')
    # img.readonly = False

    print(img.histogram())
    input()

    # d = ImageDraw.Draw(img)
    # # print(f'got draw (refcount: {sys.getrefcount(draw)})')
    # # with draw(img) as d:
    # d.rectangle([0, 0, XSIZE, YSIZE], fill='white')
    #   # d = None
    #   # del(d)
    # # print(f'after rectangle draw (refcount: {sys.getrefcount(draw)})')
    # # del(d)

    print(img.histogram())
    input()

    for n in range(randint(0, XSIZE * YSIZE)):
      x = randint(0, XSIZE-1)
      y = randint(0, YSIZE-1)
      c = randint(0x00, 0xff)
      img.putpixel((x, y), c)

    print(img.histogram())
    input()

    print(f'before del img (refcount: {sys.getrefcount(img)})')
    # img.close()
    # del(img)
    # img = None
    # del(img)

except Exception as e:
  print(e)

# try:
#   with open('test.mmap', 'r+b') as fd:
#     with mmap(fd.fileno(), 0) as mm:
#       print('got mmap')
#
#       img = Image.frombuffer(data=mm, size=(XSIZE, YSIZE), mode='L')
#       img.readonly = False
#
#       print(img.histogram())
#       input()
#
#       draw = ImageDraw.Draw(img)
#       draw.rectangle([0, 0, XSIZE, YSIZE], fill='white')
#       del(draw)
#
#       print(img.histogram())
#       input()
#
#       for n in range(randint(0, XSIZE * YSIZE)):
#         x = randint(0, XSIZE-1)
#         y = randint(0, YSIZE-1)
#         c = randint(0x00, 0xff)
#         img.putpixel((x, y), c)
#
#       print(img.histogram())
#       input()
#
#       del(img)
#
# except Exception as e:
#   print(e)
