from PIL import __version__
from PIL import Image
from PIL import ImageDraw
import numpy as np

print(__version__)

width,height=1,8
data = np.array([ord('A')]*(width*height),dtype=np.uint8) #  An 1x8 chunk composed of 'A'
print('1 data = ', data)

img = Image.frombuffer('L',(width,height), data,'raw','L',0,1)
print(img.readonly)
img.readonly = False
print('2 img = ', img.tobytes())
assert(img.getpixel((0,0)) == ord('A'))     # Assert that the data to img conversion works
draw = ImageDraw.Draw(img)
draw.rectangle((0,0,width,height),ord('B')) # Draw B's in the entire image
print('3 img = ', img.tobytes())
assert(img.getpixel((0,0)) == ord('B'))     # Assert that the drawing worked
print('4 data = ',data)
assert(data[0] == ord('B'))                 # Assert that the underlying data was changed
