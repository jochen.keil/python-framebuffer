from array import array
from fcntl import ioctl
from struct import unpack, calcsize
from collections import namedtuple

# #include <linux/fb.h>
FBIOGET_VSCREENINFO = 0x4600
FBIOGET_FSCREENINFO = 0x4602

# struct fb_fix_screeninfo {
#           char id[16];                    /* identification string eg "TT Builtin" */
#           unsigned long smem_start;       /* Start of frame buffer mem */
#                                           /* (physical address) */
#           __u32 smem_len;                 /* Length of frame buffer mem */
#           __u32 type;                     /* see FB_TYPE_*                */
#           __u32 type_aux;                 /* Interleave for interleaved Planes */
#           __u32 visual;                   /* see FB_VISUAL_*              */
#           __u16 xpanstep;                 /* zero if no hardware panning  */
#           __u16 ypanstep;                 /* zero if no hardware panning  */
#           __u16 ywrapstep;                /* zero if no hardware ywrap    */
#           __u32 line_length;              /* length of a line in bytes    */
#           unsigned long mmio_start;       /* Start of Memory Mapped I/O   */
#                                           /* (physical address) */
#           __u32 mmio_len;                 /* Length of Memory Mapped I/O  */
#           __u32 accel;                    /* Indicate to driver which     */
#                                           /*  specific chip/card we have  */
#           __u16 capabilities;             /* see FB_CAP_*                 */
#           __u16 reserved[2];              /* Reserved for future compatibility */
#   };

FB_FIX_SCREENINFO_FMT = '16sLIIIIHHHILIIH2s'
FB_FIX_SCREENINFO_FIELDS = \
'''
  id
  smem_start
  smem_len
  type
  type_aux
  visual
  xpanstep
  ypanstep
  ywrapstep
  line_length
  mmio_start
  mmio_len
  accel
  capabilities
  reserved
'''

# TODO: properly unpack `reserved`

def unpack_fb_fix_screeninfo(buf):
  fb_fix_screeninfo = namedtuple('fb_fix_screeninfo', FB_FIX_SCREENINFO_FIELDS)
  return fb_fix_screeninfo._make(unpack(FB_FIX_SCREENINFO_FMT, buf))

def get_fscreeninfo(framebuffer):
  with open(framebuffer, 'rb') as fd:
    return get_fscreeninfo_fd(fd)

def get_fscreeninfo_fd(fd):
  buf = array('b', [0] * calcsize(FB_FIX_SCREENINFO_FMT))
  ioctl(fd, FBIOGET_FSCREENINFO, buf, True)
  return unpack_fb_fix_screeninfo(buf)

# struct fb_bitfield {
#           __u32 offset;                   /* beginning of bitfield        */
#           __u32 length;                   /* length of bitfield           */
#           __u32 msb_right;                /* != 0 : Most significant bit is */
#                                           /* right */
#   };

# struct fb_var_screeninfo {
#           __u32 xres;                     /* visible resolution           */
#           __u32 yres;
#           __u32 xres_virtual;             /* virtual resolution           */
#           __u32 yres_virtual;
#           __u32 xoffset;                  /* offset from virtual to visible */
#           __u32 yoffset;                  /* resolution                   */
#
#           __u32 bits_per_pixel;           /* guess what                   */
#           __u32 grayscale;                /* 0 = color, 1 = grayscale,    */
#                                           /* >1 = FOURCC                  */
#           struct fb_bitfield red;         /* bitfield in fb mem if true color, */
#           struct fb_bitfield green;       /* else only length is significant */
#           struct fb_bitfield blue;
#           struct fb_bitfield transp;      /* transparency                 */
#
#           __u32 nonstd;                   /* != 0 Non standard pixel format */
#
#           __u32 activate;                 /* see FB_ACTIVATE_*            */
#
#           __u32 height;                   /* height of picture in mm    */
#           __u32 width;                    /* width of picture in mm     */
#
#           __u32 accel_flags;              /* (OBSOLETE) see fb_info.flags */
#
#           /* Timing: All values in pixclocks, except pixclock (of course) */
#           __u32 pixclock;                 /* pixel clock in ps (pico seconds) */
#           __u32 left_margin;              /* time from sync to picture    */
#           __u32 right_margin;             /* time from picture to sync    */
#           __u32 upper_margin;             /* time from sync to picture    */
#           __u32 lower_margin;
#           __u32 hsync_len;                /* length of horizontal sync    */
#           __u32 vsync_len;                /* length of vertical sync      */
#           __u32 sync;                     /* see FB_SYNC_*                */
#           __u32 vmode;                    /* see FB_VMODE_*               */
#           __u32 rotate;                   /* angle we rotate counter clockwise */
#           __u32 colorspace;               /* colorspace for FOURCC-based modes */
#           __u32 reserved[4];              /* Reserved for future compatibility */
#   };

# TODO: properly unpack `fb_bitfield` and `reserved`

FB_VAR_SCREENINFO_FMT = 'IIIIIIII' + 4*'8s' + 'IIIIIIIIIIIIIIII' + 1*'8s'
FB_VAR_SCREENINFO_FIELDS = \
'''
xres
yres
xres_virtual
yres_virtual
xoffset
yoffset
bits_per_pixel
grayscale
fb_bitfield_red
fb_bitfield_green
fb_bitfield_blue
fb_bitfield_transp
nonstd
activate
height
width
accel_flags
pixclock
left_margin
right_margin
upper_margin
lower_margin
hsync_len
vsync_len
sync
vmode
rotate
colorspace
reserved
'''

def unpack_fb_var_screeninfo(buf):
  fb_var_screeninfo = namedtuple('fb_var_screeninfo', FB_VAR_SCREENINFO_FIELDS)
  return fb_var_screeninfo._make(unpack(FB_VAR_SCREENINFO_FMT, buf))

def get_vscreeninfo(framebuffer):
  with open(framebuffer, 'rb') as fd:
    get_vscreeninfo_fd(fd)

def get_vscreeninfo_fd(fd):
  buf = array('b', [0] * calcsize(FB_VAR_SCREENINFO_FMT))
  ioctl(fd, FBIOGET_VSCREENINFO, buf, True)
  return unpack_fb_var_screeninfo(buf)
