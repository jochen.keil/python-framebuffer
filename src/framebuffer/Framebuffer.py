from PIL import Image
from mmap import mmap

from .screeninfo import get_vscreeninfo_fd

MODES = { 16: 'I;16'
        , 32: 'I'
        }

class Framebuffer:
  @staticmethod
  def open(fp: str, rotate=0, convert=False):
    fb = Framebuffer()

    fb.__fp = fp
    fb.__rotate = rotate
    fb.__convert = convert
    fb.__fd = open(fb.__fp, 'r+b')
    fb.__vscreeninfo = get_vscreeninfo_fd(fb.__fd)

    xres = fb.__vscreeninfo.xres
    yres = fb.__vscreeninfo.yres
    bits_per_pixel = fb.__vscreeninfo.bits_per_pixel

    if bits_per_pixel not in MODES:
      raise RuntimeError(f"{bits_per_pixel} bits per pixel not supported!")

    fb.__mm = mmap(fb.__fd.fileno(), int(xres * yres * bits_per_pixel / 8))
    fb.__img = Image.frombuffer(data=fb.__mm, size=(xres, yres), mode=MODES[bits_per_pixel])
    fb.__img.readonly = False

    return fb

  def close(self):
    self.__img.close()
    self.__mm.close()
    self.__fd.close()

  def __enter__(self):
    return self

  def __exit__(self, exc_type, exc_val, exc_tb):
    self.close()

  def paste(self, img: Image):
    tmp = img.rotate(self.__rotate)
    if self.__convert:
      tmp.convert(self.__img.mode)
    self.__img.paste(tmp)

  @property
  def img(self):
    return self.__img

  @property
  def vscreeninfo(self):
    return self.__vscreeninfo
