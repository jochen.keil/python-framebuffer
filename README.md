# Framebuffer Image

FramebufferImage provides a wrapper around `PIL.Image` on Framebuffer devices.

The framebuffer is mmap'ed and then a write-through Image object is instantiated
for easy drawing, e.g. with ImageDraw. One important caveat though: Any Object
that references the `Image` instance must be `del`eted, e.g.:

```
draw = ImageDraw.Draw(img)
del(draw)
```
